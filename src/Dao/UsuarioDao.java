package Dao;

import Basicas.Usuario;
import Interfaces.iUsuarioDao;
import dados.DadosUsuario;

public class UsuarioDao implements iUsuarioDao<Object>{
	private DadosUsuario dados;

	public UsuarioDao(){
		dados = dados == null ? new DadosUsuario() : dados; 
	}
	@Override
	public void Create(Object obj) {
		
		dados.create((Usuario)obj);
	}

	@Override
	public void Update(Object obj) {
		// TODO Auto-generated method stub
		dados.update((Usuario) obj);
	}

	@Override
	public Object Read(String usuario) {
		// TODO Auto-generated method stub
		Usuario usu = dados.search(usuario);
		
		return usu;
	}

	public void close(){
		dados.close();
	}
	
	public String searchUser(Usuario usuario){
		return dados.searchUser(usuario);
	}
	

}
