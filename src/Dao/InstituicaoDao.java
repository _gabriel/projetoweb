package Dao;

import Basicas.Instituicao;
import Basicas.Usuario;
import Interfaces.iInstituicaoDao;
import dados.DadosInstituicao;

public class InstituicaoDao implements iInstituicaoDao<Object>{
	private DadosInstituicao dados;

	public InstituicaoDao(){
		dados = dados == null ? new DadosInstituicao() : dados; 
	}
	
	public void Create(Object obj) {
		dados.create((Instituicao)obj);
	}

	public void Update(Object obj) {
		dados.update((Instituicao)obj);
	}
	public Object Read() {
		// TODO Auto-generated method stub
		return  dados.search();
	}
	public void close(){
		dados.close();
	}
	
	public Object Read(String cnpj, String nomeFantasia) {
		// TODO Auto-generated method stub
		
		return  dados.search(cnpj, nomeFantasia);
	}

	public String searchInstituicao(Instituicao inst){
		return dados.searchInstituicao(inst);
	}
}
