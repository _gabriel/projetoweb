package Basicas;

public class Endereco {
	
	private String rua, cidade, bairro, estado, cep, numero;
	private int  id;
	/**
	 * @return the rua
	 */
	public String getRua() {
		return rua;
	}
	/**
	 * 
	 * @return the cidade
	 */
	public String getCidade() {
		return cidade;
	}
	/**
	 * @return the bairro
	 */
	public String getBairro() {
		return bairro;
	}
	/**
	 * @return the estado
	 */
	public String getEstado() {
		return estado;
	}
	/**
	 * @return the cep
	 */
	public String getCep() {
		return cep;
	}
	/**
	 * @return the numero
	 */
	public String getNumero() {
		return numero;
	}
	/**
	 * @param rua the rua to set
	 */
	public void setRua(String rua) {
		this.rua = rua;
	}
	/**
	 * @param cidade the cidade to set
	 */
	public void setCidade(String cidade) {
		this.cidade = cidade;
	}
	/**
	 * @param bairro the bairro to set
	 */
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
	/**
	 * @param estado the estado to set
	 */
	public void setEstado(String estado) {
		this.estado = estado;
	}
	/**
	 * @param cep the cep to set
	 */
	public void setCep(String cep) {
		this.cep = cep;
	}
	/**
	 * @param numero the numero to set
	 */
	public void setNumero(String numero) {
		this.numero = numero;
	}
	
	public void setId(int id){
		this.id = id;
	}
	
	public int getId(){
		return id;
	}
	/**
	 * @param rua
	 * @param cidade
	 * @param bairro
	 * @param estado
	 * @param cep
	 * @param numero
	 */
	public Endereco(String rua, String cidade, String bairro, String estado, String cep, String numero) {
		this.rua = rua;
		this.cidade = cidade;
		this.bairro = bairro;
		this.estado = estado;
		this.cep = cep;
		this.numero = numero;
	}
	
	public Endereco(){}
	
	
	
}
