package Basicas;

public class Instituicao {
	private String cnpj, nomeFantasia, RazaoSocial, responsavel, telefone, cpfResponsavel, status;
	private Endereco endereco;
	private int id;
	/**
	 * @return the cnpj
	 */
	public String getCnpj() {
		return cnpj;
	}
	/**
	 * @return the nomeFantasia
	 */
	public String getNomeFantasia() {
		return nomeFantasia;
	}
	/**
	 * @return the razaoSocial
	 */
	public String getRazaoSocial() {
		return RazaoSocial;
	}
	/**
	 * @return the responsavel
	 */
	public String getResponsavel() {
		return responsavel;
	}
	/**
	 * @return the telefone
	 */
	public String getTelefone() {
		return telefone;
	}
	/**
	 * @return the cpfResponsavel
	 */
	public String getCpfResponsavel() {
		return cpfResponsavel;
	}
	/**
	 * @return the endereco
	 */
	public Endereco getEndereco() {
		return endereco;
	}
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * @param cnpj 
	 */
	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
	/**
	 * @param nomeFantasia the nomeFantasia to set
	 */
	public void setNomeFantasia(String nomeFantasia) {
		this.nomeFantasia = nomeFantasia;
	}
	/**
	 * @param razaoSocial the razaoSocial to set
	 */
	public void setRazaoSocial(String razaoSocial) {
		RazaoSocial = razaoSocial;
	}
	/**
	 * @param responsavel the responsavel to set
	 */
	public void setResponsavel(String responsavel) {
		this.responsavel = responsavel;
	}
	/**
	 * @param telefone the telefone to set
	 */
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	/**
	 * @param cpfResponsavel the cpfResponsavel to set
	 */
	public void setCpfResponsavel(String cpfResponsavel) {
		this.cpfResponsavel = cpfResponsavel;
	}
	/**
	 * @param endereco the endereco to set
	 */
	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @param cnpj
	 * @param nomeFantasia
	 * @param razaoSocial
	 * @param responsavel
	 * @param telefone
	 * @param cpfResponsavel
	 * @param endereco
	 * @param id
	 */
	public Instituicao(String cnpj, String nomeFantasia, String razaoSocial, String responsavel, String telefone,
			String cpfResponsavel, Endereco endereco) {
		this.cnpj = cnpj;
		this.nomeFantasia = nomeFantasia;
		RazaoSocial = razaoSocial;
		this.responsavel = responsavel;
		this.telefone = telefone;
		this.cpfResponsavel = cpfResponsavel;
		this.endereco = endereco;
		
	}
	/**
	 * 
	 */
	public Instituicao() {
	}
	
	
}
