package Interfaces;

//Interface padr�o do projeto
public interface iInstituicaoDao<T> {

	void Create(T obj);
	void Update(T obj);
	T Read(String cnpj, String nomeFantasia);
	T Read();
}