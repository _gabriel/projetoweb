package Interfaces;

public interface iUsuarioDao<T> {
	void Create(T obj);
	void Update(T obj);
	Object Read(String usuario);
}
