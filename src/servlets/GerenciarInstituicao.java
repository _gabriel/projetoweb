package servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Basicas.Endereco;
import Basicas.Instituicao;
import Basicas.Usuario;
import Dao.InstituicaoDao;
import Dao.UsuarioDao;

/**
 * Servlet implementation class GerenciarInstituicao
 */
@WebServlet("/GerenciarInstituicao")
public class GerenciarInstituicao extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public GerenciarInstituicao() {
		super();
		// TODO Auto-generated constructor stub
	}

	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String cnpj, nomeFantasia, razaoSocial, responsavel, cpfResponsavel, rua, bairro, numero, cep, estado, telefone, idInstituicao, idEndereco,
				cidade = "";
		String acao = "";
		RequestDispatcher rd = null;
		Endereco end;
		if (request.getParameter("Cadastrar") != null)
			acao = "cadastrar";
		else if (request.getParameter("Excluir") != null)
			acao = "excluir";
		else if (request.getParameter("Atualizar") != null)
			acao = "atualizar";
		else {
			rd = request.getRequestDispatcher("cadastro.jsp");
			rd.forward(request, response);
			return;
		}
		// if(acao == null)
		// return;

		InstituicaoDao iDao = null;
		String retorno = "ok";
		Instituicao inst = null;
		cnpj = request.getParameter("cnpj");

		nomeFantasia = request.getParameter("nomeFantasia").toUpperCase();
		razaoSocial = request.getParameter("razaoSocial").toUpperCase();
		responsavel = request.getParameter("responsavel").toUpperCase();
		cpfResponsavel = request.getParameter("cpfResponsavel");
		idInstituicao = request.getParameter("idInstituicao");
		idEndereco= request.getParameter("idEndereco");
		
		rua = request.getParameter("rua").toUpperCase();
		bairro = request.getParameter("bairro").toUpperCase();
		numero = request.getParameter("numero");
		cep = request.getParameter("cep");

		estado = request.getParameter("estado");
		telefone = request.getParameter("telefone");

		cidade = request.getParameter("cidade").toUpperCase();
		boolean retornoCnpj = true;
		switch (acao) {
		case "cadastrar":
			
			end = new Endereco(rua, cidade, bairro, estado, cep, numero);
			inst = new Instituicao(cnpj, nomeFantasia, razaoSocial, responsavel, telefone, cpfResponsavel, end);
			iDao = new InstituicaoDao();
			inst = this.removerFormatacao(inst);
			retorno = this.validaInsticuicao(inst);
			
			if (!(retorno.equals("ok"))) {
				retorno = "O campo " + retorno + " est� vazio.";
				request.setAttribute("retorno", retorno);
				rd = request.getRequestDispatcher("cadastro.jsp");
				break;
			}
			 retornoCnpj = this.isCNPJ(inst.getCnpj());
			if (retornoCnpj == false) {
				retorno = "O cnpj " + inst.getCnpj() + " � inv�lido.";
				request.setAttribute("retorno", retorno);
				rd = request.getRequestDispatcher("cadastro.jsp");
				break;
			}
			 retornoCnpj = this.isCpf(inst.getCpfResponsavel());
			if (retornoCnpj == false) {
				retorno = "O cpf " + inst.getCnpj() + " � inv�lido.";
				request.setAttribute("retorno", retorno);
				rd = request.getRequestDispatcher("cadastro.jsp");
				break;
			}
			retorno = iDao.searchInstituicao(inst);
			if (!(retorno.equals(""))) {
				retorno = "J� existe uma instituicao com o cnpj: " + inst.getCnpj() + ".";
				request.setAttribute("retCadastro", retorno);
				rd = request.getRequestDispatcher("cadastro.jsp");
				break;
			}

			iDao.Create(inst);
			iDao.close();
			
			request.setAttribute("retorno", retorno);
			rd = request.getRequestDispatcher("cadastro.jsp");
			break;
			
		case "atualizar":
			
			end = new Endereco(rua, cidade, bairro, estado, cep, numero);
			inst = new Instituicao(cnpj, nomeFantasia, razaoSocial, responsavel, telefone, cpfResponsavel, end);
			
			if (Integer.parseInt(idInstituicao) == 0){
			
				rd = request.getRequestDispatcher("cadastro.jsp");
				rd.forward(request, response);
				return;
			}
			inst.setId(Integer.parseInt(idInstituicao));
			inst.getEndereco().setId(Integer.parseInt(idEndereco));
			inst.setStatus("ATIVO");
			iDao = new InstituicaoDao();
			inst = this.removerFormatacao(inst);
			retorno = this.validaInsticuicao(inst);
			
			if (!(retorno.equals("ok"))) {
				retorno = "O campo " + retorno + " est� vazio.";
				request.setAttribute("retorno", retorno);
				rd = request.getRequestDispatcher("cadastro.jsp");
				break;
			}
			 retornoCnpj = this.isCNPJ(inst.getCnpj());
			if (retornoCnpj == false) {
				retorno = "O cnpj " + inst.getCnpj() + " � inv�lido.";
				request.setAttribute("retorno", retorno);
				rd = request.getRequestDispatcher("cadastro.jsp");
				break;
			}
			 retornoCnpj = this.isCpf(inst.getCpfResponsavel());
			if (retornoCnpj == false) {
				retorno = "O cpf " + inst.getCnpj() + " � inv�lido.";
				request.setAttribute("retorno", retorno);
				rd = request.getRequestDispatcher("cadastro.jsp");
				break;
			}
			
			retorno = iDao.searchInstituicao(inst);
			
			
			inst = removerFormatacao(inst);
			iDao.Update(inst);
			rd = request.getRequestDispatcher("cadastro.jsp");
			break;
			
		case "excluir":
			
			end = new Endereco(rua, cidade, bairro, estado, cep, numero);
			inst = new Instituicao(cnpj, nomeFantasia, razaoSocial, responsavel, telefone, cpfResponsavel, end);
			if (Integer.parseInt(idInstituicao) == 0){
				
				rd = request.getRequestDispatcher("cadastro.jsp");
				rd.forward(request, response);
				return;
			}
			inst.setId(Integer.parseInt(idInstituicao));
			inst.getEndereco().setId(Integer.parseInt(idEndereco));
			inst.setStatus("BLOQUEADO");
			iDao = new InstituicaoDao();
			inst = removerFormatacao(inst);
			iDao.Update(inst);
			rd = request.getRequestDispatcher("cadastro.jsp");
			break;
			
		}
		rd.forward(request, response);

	}

	private Instituicao removerFormatacao(Instituicao inst) {
		inst.setCnpj(inst.getCnpj().replace(".", "").replace("/", "").replace("-", ""));
		inst.setCpfResponsavel(inst.getCpfResponsavel().replace(".", "").replace("-", ""));
		inst.setTelefone(inst.getTelefone().replace("-", "").replace(".", ""));
		inst.getEndereco().setCep(inst.getEndereco().getCep().replace(".", "").replace("-", ""));

		return inst;
	}

	private String validaInsticuicao(Instituicao inst) {
		String retorno = "ok";
		if (inst.getCnpj().equals(""))
			retorno = "cnpj";
		else if (inst.getNomeFantasia().equals(""))
			retorno = "nome fantasia";
		else if (inst.getRazaoSocial().equals(""))
			retorno = "raz�o social";
		else if (inst.getResponsavel().equals(""))
			retorno = "respons�vel";
		else if (inst.getCpfResponsavel().equals(""))
			retorno = "cpf do respons�vel";
		else if (inst.getEndereco().getRua().equals(""))
			retorno = "rua";
		else if (inst.getEndereco().getNumero().equals(""))
			retorno = "numero";
		else if (inst.getEndereco().getBairro().equals(""))
			retorno = "bairro";
		else if (inst.getEndereco().getCep().equals(""))
			retorno = "cep";
		else if (inst.getTelefone().equals(""))
			retorno = "telefone";
		else if (inst.getEndereco().getCidade().equals(""))
			retorno = "cidade";
		else if (inst.getEndereco().getEstado().equals(""))
			retorno = "estado";
		return retorno;
	}

	private static boolean isCpf(String cpf) {
		if (cpf.equals("00000000000") || cpf.equals("11111111111") || cpf.equals("22222222222")
				|| cpf.equals("33333333333") || cpf.equals("44444444444") || cpf.equals("55555555555")
				|| cpf.equals("66666666666") || cpf.equals("77777777777") || cpf.equals("88888888888")
				|| cpf.equals("99999999999") || (cpf.length() != 11))
			return (false);

		char dig10, dig11;
		int sm, i, r, num, peso;

		sm = 0;
		peso = 10;
		for (i = 0; i < 9; i++) {
			num = (int) (cpf.charAt(i) - 48);
			sm = sm + (num * peso);
			peso = peso - 1;
		}

		r = 11 - (sm % 11);
		if ((r == 10) || (r == 11))
			dig10 = '0';
		else
			dig10 = (char) (r + 48);
		sm = 0;
		peso = 11;

		for (i = 0; i < 10; i++) {
			num = (int) (cpf.charAt(i) - 48);
			sm = sm + (num * peso);
			peso = peso - 1;
		}

		r = 11 - (sm % 11);

		if ((r == 10) || (r == 11))
			dig11 = '0';
		else
			dig11 = (char) (r + 48);
		if ((dig10 == cpf.charAt(9)) && (dig11 == cpf.charAt(10)))
			return (true);
		else
			return (false);

	}

	private static boolean isCNPJ(String CNPJ) {

		if (CNPJ.equals("00000000000000") || CNPJ.equals("11111111111111") || CNPJ.equals("22222222222222")
				|| CNPJ.equals("33333333333333") || CNPJ.equals("44444444444444") || CNPJ.equals("55555555555555")
				|| CNPJ.equals("66666666666666") || CNPJ.equals("77777777777777") || CNPJ.equals("88888888888888")
				|| CNPJ.equals("99999999999999") || (CNPJ.length() != 14))
			return (false);

		char dig13, dig14;
		int sm, i, r, num, peso;
		sm = 0;
		peso = 2;

		for (i = 11; i >= 0; i--) {

			num = (int) (CNPJ.charAt(i) - 48);
			sm = sm + (num * peso);
			peso = peso + 1;
			if (peso == 10)
				peso = 2;
		}

		r = sm % 11;
		if ((r == 0) || (r == 1))
			dig13 = '0';
		else
			dig13 = (char) ((11 - r) + 48);
		sm = 0;
		peso = 2;

		for (i = 12; i >= 0; i--) {
			num = (int) (CNPJ.charAt(i) - 48);
			sm = sm + (num * peso);
			peso = peso + 1;
			if (peso == 10)
				peso = 2;
		}

		r = sm % 11;
		if ((r == 0) || (r == 1))
			dig14 = '0';
		else
			dig14 = (char) ((11 - r) + 48);
		if ((dig13 == CNPJ.charAt(12)) && (dig14 == CNPJ.charAt(13)))
			return (true);
		else
			return (false);
	}
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
