package servlets;

import java.io.IOException;
import java.security.DigestInputStream;
import java.security.DigestOutputStream;

import Basicas.Usuario;
import Dao.UsuarioDao;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.websocket.Session;

import com.mysql.fabric.proto.xmlrpc.DigestAuthentication;

/**
 * Servlet implementation class GerenciarUsuario
 */
@WebServlet("/GerenciarUsuario")
public class GerenciarUsuario extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GerenciarUsuario() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String acao = request.getParameter("acao");
		if(acao == null)
			return;
		RequestDispatcher rd = null;
		String usuario, senha, nome, email = "";
		UsuarioDao uDao = null;
		String retorno = "ok";
		Usuario user = null;
		switch(acao){
		case "cadastro":
			usuario = request.getParameter("cUsuario");
			senha = request.getParameter("ConfSenha");
			nome = request.getParameter("cNome");
			email = request.getParameter("ConfEmail");

			user = new Usuario();
			user.setEmail(email);
			user.setSenha(senha);
			user.setNome(nome);
			user.setUsuario(usuario);
			retorno = this.validaUsuario(user);
			if(!(retorno.equals("ok"))){
				retorno = "O campo " + retorno + " est� vazio.";
				request.setAttribute("retorno", retorno);
				rd = request.getRequestDispatcher("CadastroUsuario.jsp");
				break;
			}
			uDao = new UsuarioDao();
			retorno = uDao.searchUser(user);
			if(!(retorno.equals(""))){
				retorno = "O usuario " +user.getUsuario()+ " j� existe.";
				request.setAttribute("retorno", retorno);
				rd = request.getRequestDispatcher("CadastroUsuario.jsp");
				break;
			}
			uDao.Create(user);
			uDao.close();
			request.setAttribute("retorno", retorno);
			rd = request.getRequestDispatcher("index.jsp");
			break;
		case "login":
			usuario = request.getParameter("cUsuario");
			senha = request.getParameter("cSenha");
			uDao = new UsuarioDao();
			user = (Usuario)uDao.Read(usuario);
			
			if(user == null || !(user.getSenha().equals(senha))){
				
				retorno = "usuario ou senha incorretos.";
				request.setAttribute("retorno", retorno);
				rd = request.getRequestDispatcher("index.jsp");
				break;
			}
			request.setAttribute("usuario", usuario);
			rd = request.getRequestDispatcher("cadastro.jsp");
			break;
		}
		rd.forward(request, response);
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	private String validaUsuario(Usuario usuario){
		String retorno = "ok";
		if(usuario.getNome().equals(""))
			retorno = "Nome";
		if(usuario.getUsuario().equals(""))
			retorno = "Usuario";
		if(usuario.getSenha().equals(""))
			retorno = "Senha";
		if(usuario.getEmail().equals(""))
			retorno = "Email";
		return retorno;
	}
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
