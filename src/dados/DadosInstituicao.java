package dados;

import java.sql.*;
import java.util.ArrayList;

import Basicas.*;
import conexao.*;

public class DadosInstituicao {
	private Instituicao inst = null;
	private Connection conn = Connect.getConnection();
	private String query;
	private PreparedStatement st;

	public Instituicao search(String cnpj, String nomeFantasia) {
		inst = new Instituicao();
		try {
			// pesquisando a instituição no banco de acordo com o cnpj

			query = "SELECT * FROM INSTITUICAO as t1 ";
			query += "INNER JOIN ENDERECO as t2 ";
			query += "ON (t2.INSTITUICAO_id = t1.ID) ";
			query += "WHERE T1.STATUS = 'ATIVO' ";

			if (!(cnpj.equals("")))
				query += " AND T1.CNPJ = '" + cnpj + "' ";
			if (!(nomeFantasia.equals("")))
				query += " AND T1.NOMEFANTASIA LIKE '%" + nomeFantasia + "%'";
			
			st = conn.prepareStatement(query);
			
			ResultSet rs = st.executeQuery();
			
			// Instanciando os dados do banco no objeto
			rs.next();
			
			inst = instanciar(rs);
		} catch (Exception e) {
			return null;
		}
		return inst;
	}

	public ArrayList<Instituicao> search() {
		ArrayList<Instituicao> lista = new ArrayList<Instituicao>();
		try {
			// pesquisando todas as instituições no banco
			query = "SELECT * FROM INSTITUICAO as t1 ";
			query += "INNER JOIN ENDERECO as t2 ";
			query += "ON (t2.INSTITUICAO_id = t1.ID) ";
			query += " WHERE T1.STATUS = 'ATIVO'";
			
			st = conn.prepareStatement(query);
			ResultSet rs = st.executeQuery();

			while (rs.next()) {
				lista.add(instanciar(rs));
			}
		} catch (Exception e) {
			return null;
		}
		return lista;

	}

	/**
	 * Function: Create Create a new instance of a institution on the database
	 * 
	 * @Param inst
	 */

	public void create(Instituicao inst) {
		try {
			query = "INSERT INTO INSTITUICAO ";
			query += "(";
			query += "CNPJ,";
			query += "RESPONSAVEL,";
			query += "RAZAOSOCIAL,";
			query += "TELEFONE,";
			query += "NOMEFANTASIA,";
			query += "CPFRESPONSAVEL, STATUS)";
			query += "VALUES";
			query += " ('" + inst.getCnpj() + "','" + inst.getResponsavel() + "','" + inst.getRazaoSocial() + "','"
					+ inst.getTelefone() + "','" + inst.getNomeFantasia() + "','" + inst.getCpfResponsavel()
					+ "', 'ATIVO')";
			st = conn.prepareStatement(query);

			st.executeUpdate();
			int numero = Integer.parseInt(searchInstituicao(inst));
			
			
			query = "INSERT INTO ENDERECO ";
			query += "(";
			query += "RUA,";
			query += "CIDADE,";
			query += "BAIRRO,";
			query += "ESTADO,";
			query += "CEP,";
			query += "NUMERO,";
			query += "INSTITUICAO_id)";
			query += "VALUES";
			query += "('" + inst.getEndereco().getRua() + "','" + inst.getEndereco().getCidade() + "','"
					+ inst.getEndereco().getBairro() + "','" + inst.getEndereco().getEstado() + "','"
					+ inst.getEndereco().getCep() + "'," + inst.getEndereco().getNumero() + "," + numero + ")";

			st = conn.prepareStatement(query);

			st.executeUpdate();

		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Function Update Update a instance of a institution in the database
	 */
	public void update(Instituicao inst) {
		try {

			query = "UPDATE INSTITUICAO ";
			query += "SET ";
			query += "CNPJ = '" + inst.getCnpj() + "',";
			query += "RESPONSAVEL = '" + inst.getResponsavel() + "',";
			query += "RAZAOSOCIAL = '" + inst.getRazaoSocial() + "',";
			query += "TELEFONE = '" + inst.getTelefone() + "',";
			query += "NOMEFANTASIA= '" + inst.getNomeFantasia() + "',";
			query += "STATUS= '" + inst.getStatus() + "',";
			query += "CPFRESPONSAVEL = '" + inst.getCpfResponsavel() + "' ";
			query += " WHERE ";
			query += "ID = " + inst.getId() + "";
			st = conn.prepareStatement(query);
			
			st.executeUpdate();

			query = "UPDATE ENDERECO ";
			query += " SET ";
			query += "RUA = '" + inst.getEndereco().getRua() + "',";
			query += "CIDADE = '" + inst.getEndereco().getCidade() + "',";
			query += "BAIRRO = '" + inst.getEndereco().getBairro() + "',";
			query += "ESTADO = '" + inst.getEndereco().getEstado() + "',";
			query += "CEP= '" + inst.getEndereco().getCep() + "',";
			query += "NUMERO = " + inst.getEndereco().getNumero() + "";
			query += " WHERE ";
			query += "ID_ENDERECO = " + inst.getEndereco().getId() + "";
			
			st = conn.prepareStatement(query);

			st.executeUpdate();
		} catch (Exception e) {

		}
	}

	private Instituicao instanciar(ResultSet rs) {
		Instituicao in = new Instituicao();
		Endereco en = new Endereco();
		try {
			in.setId(rs.getInt("ID"));
			in.setRazaoSocial(rs.getString("RAZAOSOCIAL"));
			in.setCnpj(rs.getString("CNPJ"));
			in.setResponsavel(rs.getString("RESPONSAVEL"));
			in.setTelefone(rs.getString("TELEFONE"));
			in.setNomeFantasia(rs.getString("NOMEFANTASIA"));
			in.setCpfResponsavel(rs.getString("CPFRESPONSAVEL"));
			in.setStatus(rs.getString("STATUS"));

			en.setRua(rs.getString("RUA"));
			en.setCidade(rs.getString("CIDADE"));
			en.setBairro(rs.getString("BAIRRO"));
			en.setEstado(rs.getString("ESTADO"));
			en.setCep(rs.getString("CEP"));
			en.setNumero(Integer.toString(rs.getInt("NUMERO")));
			en.setId(rs.getInt("ID_ENDERECO"));
			in.setEndereco(en);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		return in;
	}

	public void close() {
		try {
			conn.close();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	public String searchInstituicao(Instituicao inst) {
		String retorno = "";
		try {
			query = "SELECT ID FROM INSTITUICAO WHERE CNPJ = '" + inst.getCnpj() + "' AND STATUS <> 'BLOQUEADO'";
			st = conn.prepareStatement(query);
			st.execute();
			ResultSet rs = st.executeQuery();
			if (rs.next())
				retorno = Integer.toString(rs.getInt("ID"));
			// usuario = instanciar(rs);
		} catch (SQLException u) {
			throw new RuntimeException(u);
		}
		return retorno;
	}

}
