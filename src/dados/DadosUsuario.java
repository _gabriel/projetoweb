package dados;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import Basicas.Usuario;
import conexao.Connect;

public class DadosUsuario {
	private Usuario usuario = null;
	private Connection conn = Connect.getConnection();
	private String query;
	private PreparedStatement st;

	public Usuario search(String user) {
		try {
			query = "SELECT * FROM USUARIO WHERE LOGIN = '" + user + "'";
			
			st = conn.prepareStatement(query);

			ResultSet rs = st.executeQuery();

			rs.next();
			usuario = instanciar(rs);
			
		} catch (Exception e) {
			e.printStackTrace();
		} 
		return usuario;
	}

	public void update(Usuario usuario) {
		try {
			query = "UPDATE USUARIO SET SENHA = '" + usuario.getSenha() + "' WHERE LOGIN = '" + usuario.getUsuario()
					+ "'";
			st.executeUpdate();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public void create(Usuario usuario) {

		try {
			query = "INSERT INTO USUARIO (NOME, LOGIN, SENHA, EMAIL) VALUES " + "('" + usuario.getNome() + "'," + "'"
					+ usuario.getUsuario() + "'," + "'" + usuario.getSenha() + "'," + "'" + usuario.getEmail() + "')";
			st = conn.prepareStatement(query);
			st.execute();
			// usuario = instanciar(rs);
		} catch (SQLException u) {
			throw new RuntimeException(u);
		}
	}

	public String searchUser(Usuario usuario) {
		String retorno = "";
		try {
			query = "SELECT LOGIN FROM USUARIO WHERE LOGIN = '" + usuario.getUsuario() + "'";

			st = conn.prepareStatement(query);
			st.execute();
			ResultSet rs = st.executeQuery();
			if (rs.next())
				retorno = "existe";
			// usuario = instanciar(rs);
		} catch (SQLException u) {
			throw new RuntimeException(u);
		}
		return retorno;
	}

	private Usuario instanciar(ResultSet rs) {
		Usuario u = new Usuario();
		try {
			u.setId(rs.getInt("ID"));
			u.setUsuario(rs.getString("LOGIN"));
			u.setSenha(rs.getString("SENHA"));
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		return u;
	}

	public void close() {
		try {
			conn.close();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
