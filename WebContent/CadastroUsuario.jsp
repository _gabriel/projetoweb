<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<style type="text/css">
<!--
@import url("css/estilo.css");
-->
</style>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="pt-br">
<head>

<script src="js/estilo.js"></script>
<meta charset="UTF-8">
<meta name="Projeto" content="Projeto Web">
<title>Cadastro de Usuario</title>
<link rel="stylesheet" type="text/css" href="css/estilo.css">


</head>

<body>
<%
String retorno = (String)request.getAttribute("retorno");

%>
<c:if test="${not empty retorno and retorno != 'ok' }">
	<script type="text/javascript">
		alert('<% out.print(retorno); %>');
	</script>
</c:if>

	<div class="container">
		<div class="header">
			<img src="img/Banner.jpg" width="970" height="150"
				style="background-color: #C6D580; display: block;" />
		</div>

		<div id="tudoIndex">
			
			<nav id="pesq">
			<h1>Cadastro de Usuario</h1>
			</nav>
			<section id="corpoCadUser">
			<fieldset>
				<legend>USUARIO (*Campo Obrigatorio)</legend>
				
				<form id="formulario" action="GerenciarUsuario" method="post">
					<input type="hidden" name="acao" value="cadastro">
					<p>
						*NOME : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text"
							name="cNome" id="cNome" size="50" maxlength="40"
							placeholder="Nome" />
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						*USUARIO : <input type="text" name="cUsuario" id="cUsuario"
							size="20" maxlength="10" placeholder="Usuario" />
					</p>

					<p>
						*SENHA
						:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<input type="password" name="cSenha" id="cSenha" size="20"
							maxlength="10" placeholder="Senha" />
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						*E-MAIL : <input type="text" name="cEmail" id="cEmail" size="40"
							maxlength="30" placeholder="E-mail" />
					</p>
					<p>
						*CONF. DE SENHA : <input type="password" name="ConfSenha"
							id="ConfSenha" size="20" maxlength="10"
							placeholder="Confirma��o de Senha" />
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						*CONF. DE E-MAIL : <input type="text" name="ConfEmail"
							id="ConfEmail" size="40" maxlength="30"
							placeholder="Confirma��o de E-mail"/>
					</p>
					<br />


					<p>
						<input type="submit" name="btsubmit" value="Salvar" id="bt"	> <input type="submit" name="btsair"
							value="Sair" id="bt" onclick="history.back()" />
					</p>

				</form>
			</fieldset>
			</section>
			<div class="navRec">Programa��o Web. 2015.2 - Grupo 09 -
				Gabriel Machado Bandira; Jefferson Bruno de Assis; Jos� Claudio de
				F. Filho;</div>
			<div class="footer"></div>
		</div>
	</div>

</body>
</html>