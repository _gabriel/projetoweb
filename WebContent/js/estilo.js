  // JavaScript Document
  
  function mascara(t, mask){

	  var i = t.value.length;
	  
	  var saida = mask.substring(1,0);
	  
	  var texto = mask.substring(i);
	  
	  if(texto.substring(0,1) != saida){
		  t.value+=texto.substring(0,1);
		  
	  }
 }
  
 function formataCnpfCpf(campo){
	 var texto = campo.value;
	 if(texto.length == 14){
		 campo.value = texto.substring(0,2) + "." + texto.substring(2,5) + "." +texto.substring(5,8) + "/" + texto.substring(8,12) + "-" +texto.substring(12);
		 //01.234.567/8901-32
	 }
 }
 
 function formataCpf(campo){
	 var texto = campo.value;
	 
		 campo.value = texto.substring(0,3) + "." + texto.substring(3,6) + "." +texto.substring(6,9) + "-" + texto.substring(9);
		 //01.234.567-32
	 
 }
 
 function formataTelefone(campo){
	 var texto = campo.value;
	 if(texto.length >= 9){
		 campo.value = texto.substring(0,2) + "-" + texto.substring(2,7) + "-" +texto.substring(7) ;
		 //81-98600-0512
	 }
	 
}
 
 function formataCep(campo){
	 var texto = campo.value;
	 if(texto.length == 8){
		 campo.value = texto.substring(0,2) + "." + texto.substring(2,5) + "-" +texto.substring(5) ;
		 //81-98600-0512
	 } 
 }