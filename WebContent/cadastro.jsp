<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="Dao.InstituicaoDao"%>
<%@page import="org.apache.jasper.tagplugins.jstl.core.ForEach"%>
<%@page import="java.util.*"%>
<%@page import="Basicas.*"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<style type="text/css">
<!--
@import url("css/estilo.css");
-->
</style>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<script src="js/estilo.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Cadastro Institucional</title>
<script type="text/javascript">
	function fnCarregaDados(a){
		
		document.getElementById("cCnpj").value = a.cells[0].innerHTML.trim();
		
		formataCnpfCpf(document.getElementById("cCnpj"));
		document.getElementById("cNomeFan").value = a.cells[1].innerHTML.trim();
		
		document.getElementById("cRazaoSocial").value = a.cells[2].innerHTML.trim();
		
		document.getElementById("cNomeRes").value = a.cells[3].innerHTML.trim();
		document.getElementById("idInstituicao").value = a.cells[4].innerHTML.trim();
		
		document.getElementById("cTel").value = a.cells[5].innerHTML.trim();
		formataTelefone(document.getElementById("cTel"));
		
		document.getElementById("cCpfRes").value = a.cells[6].innerHTML.trim();
		formataCpf(document.getElementById("cCpfRes"));
		
		document.getElementById("cRua").value = a.cells[7].innerHTML.trim();
		
		document.getElementById("cCidade").value = a.cells[8].innerHTML.trim();
		
		document.getElementById("cBairro").value = a.cells[9].innerHTML.trim();
		
	//	document.getElementById("cEstado").value = a.cells[10].innerHTML.trim();
		
		for(i = 0; i<=document.getElementById("estado").length-1; i++){
			if(document.getElementById("estado").options[i].value == a.cells[10].innerHTML.trim()){
				document.getElementById("estado").selectedIndex = i;
				break;
			}
		}
		document.getElementById("cCep").value = a.cells[11].innerHTML.trim();
		formataCep(document.getElementById("cCep"));
		
		document.getElementById("cNumero").value = a.cells[12].innerHTML.trim();
		document.getElementById("idEndereco").value = a.cells[13].innerHTML.trim();
		
		document.getElementById("btCadastra").disabled = true;
		
	}
	function fnLimpar(){
		document.getElementById("cCnpj").value = "";
		document.getElementById("cNomeFan").value = "";
		
		document.getElementById("cRazaoSocial").value = "";
		
		document.getElementById("cNomeRes").value = "";
		
		document.getElementById("cTel").value = "";
		
		document.getElementById("cCpfRes").value = "";
		
		document.getElementById("cRua").value = "";
		
		document.getElementById("cCidade").value = "";
		
		document.getElementById("cBairro").value = "";
		
	//	document.getElementById("cEstado").value = a.cells[10].innerHTML.trim();
		document.getElementById("estado").selectedIndex = 1;	
		
		document.getElementById("cCep").value = "";
		
		document.getElementById("cNumero").value = "";
		
		document.getElementById("idInstituicao").value = "";
		document.getElementById("idEndereco").value = "";
		
		document.getElementById("btCadastra").disabled = false;
	}
	
	function SomenteNumero(e){
	    var tecla=(window.event)?event.keyCode:e.which;   
	    if((tecla>47 && tecla<58)) return true;
	    else{
	    	if (tecla==8 || tecla==0) return true;
		else  return false;
	    }
	}
	
	function SoLetra(event){
		var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
		
		var caract = new RegExp(/^[a-z/^ ]+$/i);
		var caract = caract.test(String.fromCharCode(keyCode));
		if(!caract)
		{
			
			keyCode=0;
			return false;
			}
		}
</script>
</head>
<body>

	<%
		InstituicaoDao inst = new InstituicaoDao();
		String search = (String) request.getAttribute("acaoS");
		ArrayList<Instituicao> item = new ArrayList();
		String retCadastro = (String) request.getAttribute("retCadastro");
		String retorno = (String) request.getAttribute("retorno");
		if (search == null) {
			item = (ArrayList<Instituicao>) inst.Read();

		} else {
			Instituicao instituicao = (Instituicao) request.getAttribute("instituicao");
			item.add(instituicao);
	%>
	<c:if test="${ empty instituicao  }">
		<script type="text/javascript">
		alert("N�o foi encontrado nenhum registro.");
	</script>
	</c:if>
	<%
		}
	%>
	<c:if test="${not empty retCadastro and retCadastro != 'ok' }">
		<script type="text/javascript">
		alert('<%out.print(retCadastro);%>');
	</script>
	</c:if>
	<c:if test="${not empty retorno and retorno != 'ok' }">
		<script type="text/javascript">
		alert('<%out.print(retorno);%>');
		</script>
	</c:if>
	<div class="container">
		<div class="header">
			<img src="img/Banner.jpg" width="970" height="150"
				style="background-color: #C6D580; display: block;" />
		</div>
		<section id="corpo">

		<form action='Search' method='GET'>
			<br />
			<p>
				<input name="Buscar" type="submit" value="Buscar" id="btbuscarCnpj">
				&nbsp;&nbsp;&nbsp; <input type="text" class="campo" name="cnpj"
					size="20" maxlength="18" placeholder="CNPJ" id="cbuscarCnpj"
					onKeyPress="mascara(this,'##.###.###/####-##');return SomenteNumero(event);" />
				ou <input type="text" class="campo" name="nomeFantasia" size="60"
					maxlength="40" placeholder="Nome Fantasia" id="cbuscarNome"
					onkeypress="return SoLetra(event);" />
			</p>
		</form>
		<form action='GerenciarInstituicao' method='GET'>
			<fieldset id="for">
				<legend>Cadastro da Institui��o(*Campos Obrigatorios)</legend>
				<fieldset id="forr">
					<legend>Dados Institucional</legend>
					<input type="hidden" name="idInstituicao" id="idInstituicao" /> <input
						type="hidden" name="idEndereco" id="idEndereco" />
					<p>
						*CNPJ: <input type="text" class="campo" size="20" name='cnpj'
							maxlength="18" id="cCnpj"
							onKeyPress="mascara(this,'##.###.###/####-##');return SomenteNumero(event);" />
						&nbsp;&nbsp;&nbsp;&nbsp; *NOME FANTASIA: <input
							onkeypress="return SoLetra(event);" type="text" class="campo"
							size="60" maxlength="40" name='nomeFantasia' id="cNomeFan" />
					</p>

					<p>
						*RAZ�O SOCIAL: <input type="text"
							onkeypress="return SoLetra(event);" class="campo" size="97"
							maxlength="70" id="cRazaoSocial" name='razaoSocial' />
					</p>

					<p>
						*NOME RESPONS�VEL: <input type="text"
							onkeypress="return SoLetra(event);" class="campo" size="48"
							maxlength="30" id="cNomeRes" name='responsavel' /> *CPF
						RESPONS�VEL: <input type="text" class="campo" size="15"
							maxlength="14" id="cCpfRes" name='cpfResponsavel'
							onKeyPress="mascara(this,'###.###.###-##');return SomenteNumero(event);" />
					</p>

				</fieldset>
				<fieldset id="forr">
					<legend>Endere�o</legend>

					<p>
						*RUA: <input type="text" class="campo" size="90" id="cRua"
							maxlength="60" name='rua' /> *N�: <input type="text"
							class="campo" onkeypress="return SomenteNumero(event);" size="10"
							id="cNumero" name='numero' maxlength="5" />
					</p>

					<p>
						*BAIRRO: <input type="text" class="campo" size="40" maxlength="30"
							id="cBairro" name='bairro' /> *CEP: <input type="text"
							class="campo" size="15" id="cCep" maxlength="10" name='cep'
							onKeyPress="mascara(this,'##.###-###');return SomenteNumero(event);" />
						*TEL:<input type="text" class="campo" size="15" id="cTel"
							maxlength="10" name='telefone'
							onKeyPress="mascara(this,'#####-####');return SomenteNumero(event);" />
					</p>

					<p>
						*CIDADE: <input type="text" onkeypress="return SoLetra(event);"
							class="campo" size="40" id="cCidade" maxlength="25" name="cidade" />
						*ESTADO: <select id="estado" name="estado">
							<option value="AC">Acre</option>
							<option value="AL">Alagoas</option>
							<option value="AP">Amapa</option>
							<option value="AM">Amazonas</option>
							<option value="BA">Bahia</option>
							<option value="CE">Cear�</option>
							<option value="DF">Distrito Federal</option>
							<option value="ES">Espirito Santo</option>
							<option value="GO">Goi�is</option>
							<option value="MA">Maranh�o</option>
							<option value="MT">Mato Grosso</option>
							<option value="MS">Mato Grosso do Sul</option>
							<option value="MG">Minas Gerais</option>
							<option value="PA">Par�</option>
							<option value="PB">Para�ba</option>
							<option value="PR">Paran�</option>
							<option value="PE">Pernambuco</option>
							<option value="PI">Piau�</option>
							<option value="RJ">Rio de Janeiro</option>
							<option value="RN">Rio Grande do Norte</option>
							<option value="RS">Rio Grande do Sul</option>
							<option value="RO">Rond�nia</option>
							<option value="RR">Roraima</option>
							<option value="SC">Santa Catarina</option>
							<option value="SP">S�o Paulo</option>
							<option value="SE">Sergipe</option>
							<option value="TO">Tocantins</option>
						</select>
					</p>

					<div class="footer" align="center">

						<input name="Cadastrar" type="submit" value="Cadastrar"
							id="btCadastra"> <input name="Excluir" type="submit"
							value="Excluir" id="btExcluir"> <input name="Atualizar"
							type="submit" value="Atualizar" id="btAtualizar"> <input
							name="Limpar" type="button" value="Limpar" id="btLimpar"
							onclick='fnLimpar()'>
					</div>
				</fieldset>
			</fieldset>
		</form>

		<div id="scroll">
			<table width="930px" border="1" id="tabela" align="center">
				<tr>

					<th scope="col">CNPJ</th>
					<th scope="col">NOME FANTASIA</th>
					<th scope="col">RAZ�OO SOCIAL</th>
					<th scope="col">NOME RESPONS�VEL</th>
					<th scope="col" style='display: none'>id</th>
					<th scope="col" style='display: none'>telefone</th>
					<th scope="col" style='display: none'>cpf responsavel</th>
					<th scope="col" style='display: none'>rua</th>
					<th scope="col" style='display: none'>cidade</th>
					<th scope="col" style='display: none'>bairro</th>
					<th scope="col" style='display: none'>estado</th>
					<th scope="col" style='display: none'>cep</th>
					<th scope="col" style='display: none'>numero</th>
					<th scope="col" style='display: none'>idendereco</th>
				</tr>
				<tr>
					<c:forEach var="ite" items="<%=item%>" varStatus="loop">
						<tr ondblclick="fnCarregaDados(this);">


							<td><c:out value="${ite.getCnpj()}">
								</c:out></td>
							<td><c:out value="${ite.getNomeFantasia()}">
								</c:out></td>
							<td><c:out value="${ite.getRazaoSocial()}">
								</c:out></td>
							<td><c:out value="${ite.getResponsavel()}">
								</c:out></td>
							<td style='display: none'><c:out value="${ite.getId()}">
								</c:out></td>
							<td style='display: none'><c:out
									value="${ite.getTelefone()}">
								</c:out></td>
							<td style='display: none'><c:out
									value="${ite.getCpfResponsavel()}">
								</c:out></td>
							<td style='display: none'><c:out
									value="${ite.getEndereco().getRua()}">
								</c:out></td>
							<td style='display: none'><c:out
									value="${ite.getEndereco().getCidade()}">
								</c:out></td>
							<td style='display: none'><c:out
									value="${ite.getEndereco().getBairro()}">
								</c:out></td>
							<td style='display: none'><c:out
									value="${ite.getEndereco().getEstado()}">
								</c:out></td>
							<td style='display: none'><c:out
									value="${ite.getEndereco().getCep()}">
								</c:out></td>
							<td style='display: none'><c:out
									value="${ite.getEndereco().getNumero()}">
								</c:out></td>
							<td style='display: none'><c:out
									value="${ite.getEndereco().getId()}">
								</c:out></td>


						</tr>
					</c:forEach>


				</tr>



			</table>
		</div>
		<br />
		<div class="nav">Programa��o Web. 2015.2 - Grupo 09 - Gabriel
			Machado Bandeira; Jefferson Bruno de Assis; Jos� Claudio de F. Filho;
		</div>
		<div class="footer"></div>
		</section>
	</div>

</body>


</html>
