<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="pt-br">
<head>
<script src="js/estilo.js"></script>
<meta charset="UTF-8">
<meta name="Projeto" content="Projeto Web">
<title>LOGIN</title>
<link rel="stylesheet" type="text/css" href="css/estilo.css">
<script type="text/javascript">
	function fnRedirecionaPagina(){
		cUsuario = documento.getElementById("cUsuario").value;
		cSenha = documento.getElementById("cSenha").value;
		
		if(cUsuario == "" || cUsuario == null){
			alert("Digite um usuário");
			return;
		}
		if(cSenha == "" || cSenha == null){
			alert("Digite uma senha");
			return;
		}
		
		//talvez aqui precise do caminho completo
		document.location.href = "GerenciarUsuario&cUsuario="+cUsuario+"&cSenha="+cSenha;
	}
</script>
<style type="text/css">
<!--
@import url("css/estilo.css");
-->
</style>
</head>
<%
String retorno = (String)request.getAttribute("retorno");
%>
<c:if test="${not empty retorno and retorno != 'ok' }">
	<script type="text/javascript">
		alert('<% out.print(retorno); %>');
	</script>
</c:if>
<body>

	<div class="container">
		<div class="header">
			<img src="img/Banner.jpg" width="970" height="150"
				style="background-color: #C6D580; display: block;" />
		</div>

		<div id="tudoIndex">
			<a href="CadastroUsuario.jsp">Cadastar Novo Usuario</a>
			<header id="cabecalho">
			<div></div>
			</header>
			<nav id="pesq">
			<h1>Tela de Acesso</h1>
			</nav>
			<section id="corpoIndex">
			<fieldset>
				<legend>LOGIN(*Campo Obrigatorio)</legend>
				<form id="formulario">
				<input type="hidden" name="acao" value="login">
					<p>
						*USUARIO : <input type="text" name="cUsuario" id="cUsuario"
							size="20" maxlength="10" placeholder="Usuario" />
					</p>

					<p>
						*SENHA : &nbsp;&nbsp;&nbsp; <input type="password" name="cSenha"
							id="cSenha" size="20" maxlength="10" placeholder="Senha" /> <br />
						<span id="span"> <a href="esqueceu.jsp"></a></span>
					</p>
					<h5>
						<a href="esqueceu.jsp">Esqueceu � Senha?</a>
						<h5>
							</br>
							<p>
								<input  name="btsubmit" value="Entra" id="bt"
									onclick="fnRedirecionaPagina()"> 
							</p>
				</form>
			</fieldset>
			</section>
			<div class="navRec">Programa��o Web. 2015.2 - Grupo 09 -
				Gabriel Machado Bandira; Jefferson Bruno de Assis; Jos� Claudio de
				F. Filho;</div>
			<div class="footer"></div>
		</div>
	</div>
</body>
</html>